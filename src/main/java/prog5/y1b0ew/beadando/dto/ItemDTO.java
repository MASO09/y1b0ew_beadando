package prog5.y1b0ew.beadando.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
//import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import prog5.y1b0ew.beadando.entity.ItemType;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemDTO {

    @Id
    private String id;
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private int quantity;
    @JsonProperty("image")
    private String imageUrl;

    @NotNull
    @JsonProperty("type")
    private ItemType itemType;


}
