package prog5.y1b0ew.beadando.dto;

//import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import prog5.y1b0ew.beadando.entity.Item;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {

    @Id
    private String id;
    @NotNull
    private String itemId;
    @NotNull
    private String customerName;
    @NotNull
    private String tel;
    @NotNull
    private String address;
    @NotNull
    private int quantity;

}
