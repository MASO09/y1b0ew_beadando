package prog5.y1b0ew.beadando.dto;

public enum ItemTypeDTO {
    PHONE, LAPTOP, CONSOLE, TV, CAFFE_MACHINE, OTHER;
}
