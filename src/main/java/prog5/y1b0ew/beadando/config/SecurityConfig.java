package prog5.y1b0ew.beadando.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private static final String[] AUTH_WHITELIST = { "/static/css/**", "/js/**", "/images/**", "/favicon.ico**" };



    @Bean
    public SecurityFilterChain httpBasicFilterChain(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests(authorize -> {
            authorize.antMatchers(AUTH_WHITELIST).permitAll();
            authorize.antMatchers("/list.html").hasRole("ADMIN");
            authorize.antMatchers("/save.html").hasRole("ADMIN");
            authorize.antMatchers("/orders.html").hasRole("ADMIN");
            authorize.antMatchers("PUT","/api/item").hasRole("ADMIN");
            authorize.antMatchers("DELETE","/api/item").hasRole("ADMIN");
            authorize.antMatchers("DELETE","/api/order").hasRole("ADMIN");

            authorize.anyRequest().authenticated();

        }).formLogin(form -> {
            form.loginPage("/login");
            form.defaultSuccessUrl("/list.html", true);
            form.permitAll();
        }).logout(LogoutConfigurer::permitAll);

        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
        // @formatter:off
        return new InMemoryUserDetailsManager(
                User.builder()
                    .passwordEncoder(passwordEncoder::encode)
                    .username("user")
                    .password("user")
                    .roles("USER")
                    .build(),
                User.builder()
                    .passwordEncoder(passwordEncoder::encode)
                    .username("admin")
                    .password("admin")
                    .roles("ADMIN", "USER")
                    .build()
        );

        // @formatter:on
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
