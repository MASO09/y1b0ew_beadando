package prog5.y1b0ew.beadando.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    private String id;

    private String itemId;
    private String customerName;
    private String tel;
    private String address;
    private int quantity;
}
