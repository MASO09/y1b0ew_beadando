package prog5.y1b0ew.beadando.entity;

public enum ItemType {
    PHONE, LAPTOP, CONSOLE, TV, CAFFE_MACHINE, OTHER;
}
