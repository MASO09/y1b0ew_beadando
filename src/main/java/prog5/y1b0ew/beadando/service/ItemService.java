package prog5.y1b0ew.beadando.service;

import prog5.y1b0ew.beadando.dto.ItemDTO;
import prog5.y1b0ew.beadando.dto.ItemTypeDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ItemService {
    Mono<ItemDTO> save(ItemDTO item);

    Mono<ItemDTO> findById(String id);

    Mono<Void> delete(String id);

    Mono<ItemDTO> reduceQuantity(String id, int quantity);

    Flux<ItemDTO> findByItemType(ItemTypeDTO itemTypeDTO);

    Flux<ItemDTO> listAll();

    Flux<ItemTypeDTO> listAllTypes();

}
