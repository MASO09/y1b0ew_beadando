package prog5.y1b0ew.beadando.service;

import prog5.y1b0ew.beadando.dto.OrderDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {

    Mono<OrderDTO> save(OrderDTO order);

    Mono<OrderDTO> findById(String id);

    Mono<Void> delete(String id);

    Flux<OrderDTO> listAll();

}
