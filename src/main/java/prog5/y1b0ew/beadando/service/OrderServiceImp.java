package prog5.y1b0ew.beadando.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import prog5.y1b0ew.beadando.dto.OrderDTO;
import prog5.y1b0ew.beadando.mapper.OrderMapper;
import prog5.y1b0ew.beadando.repository.OrderRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class OrderServiceImp implements OrderService{

    private final OrderRepository repository;
    private final OrderMapper mapper;

    @Override
    public Mono<OrderDTO> save(OrderDTO order) {
        return this.repository.save(this.mapper.mapOrderDTO(order))
                .map(this.mapper::mapOrder);
    }

    @Override
    public Mono<OrderDTO> findById(String id) {
        return this.repository.findById(id).map(this.mapper::mapOrder);
    }

    @Override
    public Mono<Void> delete(String id) {
        return this.repository.deleteById(id);
    }

    @Override
    public Flux<OrderDTO> listAll() {
        return this.repository.findAll().map(this.mapper::mapOrder);
    }
}
