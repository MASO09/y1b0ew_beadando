package prog5.y1b0ew.beadando.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import prog5.y1b0ew.beadando.dto.ItemDTO;
import prog5.y1b0ew.beadando.dto.ItemTypeDTO;
import prog5.y1b0ew.beadando.entity.Item;
import prog5.y1b0ew.beadando.mapper.ItemMapper;
import prog5.y1b0ew.beadando.repository.ItemRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ItemServiceImp implements ItemService{


    private final ItemRepository repository;
    private final ItemMapper mapper;

    @Override
    public Mono<ItemDTO> save(ItemDTO item) {
        return this.repository.save(this.mapper.mapItemDTO(item))
                .map(this.mapper::mapItem);
    }

    @Override
    public Mono<ItemDTO> findById(String id) {
        return this.repository.findById(id).map(this.mapper::mapItem);
    }

    @Override
    public Mono<Void> delete(String id) {
        return this.repository.deleteById(id);
    }

    @Override
    public Mono<ItemDTO> reduceQuantity(String id, int quantity) {
        return this.repository.findById(id)
                .doOnNext(temp -> temp.setQuantity(temp.getQuantity() - quantity))
                .flatMap(temp -> this.save(this.mapper.mapItem(temp)));


    }

    @Override
    public Flux<ItemDTO> findByItemType(ItemTypeDTO itemTypeDTO) {
        return this.repository.findByItemType(this.mapper.mapItemTypeDTO(itemTypeDTO))
                .map(this.mapper::mapItem);
    }

    /*
    @Override
    public Flux<ItemDTO> findByCategory(CategoryDTO category) {
        return this.repository.findAll()
                .filter(t -> t.getCategory().getName().equals(category.getName()))
                .map(this.mapper::mapItem);
    }
    */


    @Override
    public Flux<ItemDTO> listAll() {
        return this.repository.findAll().map(this.mapper::mapItem);
    }

    public Flux<ItemTypeDTO> listAllTypes(){return Flux.fromArray(ItemTypeDTO.values());}

}
