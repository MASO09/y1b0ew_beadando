package prog5.y1b0ew.beadando.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import prog5.y1b0ew.beadando.entity.Order;

public interface OrderRepository extends ReactiveMongoRepository<Order, String> {
}
