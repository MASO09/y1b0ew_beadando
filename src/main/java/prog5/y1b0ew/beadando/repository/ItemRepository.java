package prog5.y1b0ew.beadando.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import prog5.y1b0ew.beadando.entity.Item;
import prog5.y1b0ew.beadando.entity.ItemType;
import reactor.core.publisher.Flux;

public interface ItemRepository extends ReactiveMongoRepository<Item, String> {
    Flux<Item> findByItemType(ItemType itemType);
}
