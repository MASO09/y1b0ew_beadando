package prog5.y1b0ew.beadando.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import prog5.y1b0ew.beadando.dto.OrderDTO;
import prog5.y1b0ew.beadando.entity.Order;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface OrderMapper {

    OrderDTO mapOrder(Order order);

    Order mapOrderDTO(OrderDTO orderDTO);
}
