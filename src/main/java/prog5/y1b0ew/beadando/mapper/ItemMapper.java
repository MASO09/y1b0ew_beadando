package prog5.y1b0ew.beadando.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import prog5.y1b0ew.beadando.dto.ItemDTO;
import prog5.y1b0ew.beadando.dto.ItemTypeDTO;
import prog5.y1b0ew.beadando.entity.Item;
import prog5.y1b0ew.beadando.entity.ItemType;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface ItemMapper {

    ItemDTO mapItem(Item item);

    Item mapItemDTO(ItemDTO itemDTO);

    ItemType mapItemTypeDTO(ItemTypeDTO itemTypeDTO);

    ItemTypeDTO mapItemType(ItemType itemType);

}
