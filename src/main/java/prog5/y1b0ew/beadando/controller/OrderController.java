package prog5.y1b0ew.beadando.controller;

import prog5.y1b0ew.beadando.dto.OrderDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderController {

    Mono<OrderDTO> save(OrderDTO order);

    Mono<OrderDTO> findById(String id);

    Mono<Void> delete(String id);

    Flux<OrderDTO> listAll();
}
