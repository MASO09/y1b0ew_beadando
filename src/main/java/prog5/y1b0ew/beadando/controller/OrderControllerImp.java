package prog5.y1b0ew.beadando.controller;

//import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import prog5.y1b0ew.beadando.dto.ItemDTO;
import prog5.y1b0ew.beadando.dto.OrderDTO;
import prog5.y1b0ew.beadando.service.ItemService;
import prog5.y1b0ew.beadando.service.OrderService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderControllerImp implements OrderController {

    private final OrderService orderService;
    //private final ItemService itemService;

    @Override
    @PutMapping("/order")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<OrderDTO> save(@Valid @RequestBody final OrderDTO order) {
        //this.itemService.reduceQuantity(order.getItemId(), order.getQuantity());
        return this.orderService.save(order);
    }

    @Override
    @GetMapping(value = "order/{id}", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<OrderDTO> findById(@PathVariable("id") final String id) {
        return this.orderService.findById(id);
    }

    @Override
    @DeleteMapping(value = "order/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> delete(@PathVariable("id") final String id) {
        return this.orderService.delete(id);
    }

    @Override
    @GetMapping(value = "orders", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<OrderDTO> listAll() {
        return this.orderService.listAll();
    }
}
