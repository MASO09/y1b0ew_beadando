package prog5.y1b0ew.beadando.controller;

//import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import prog5.y1b0ew.beadando.dto.ItemDTO;
import prog5.y1b0ew.beadando.dto.ItemTypeDTO;
import prog5.y1b0ew.beadando.service.ItemService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemControllerImp implements ItemController {

    private final ItemService itemService;


    @Override
    @PutMapping("/item")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ItemDTO> save(@Valid @RequestBody final ItemDTO item) {
        return this.itemService.save(item);
    }

    @Override
    @GetMapping(value="item/{id}", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<ItemDTO> findById(@PathVariable("id") final String id) {
        return this.itemService.findById(id);
    }

    @Override
    @DeleteMapping("item/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> delete(@PathVariable("id") final String id) {
        return this.itemService.delete(id);
    }

    @Override
    public Mono<ItemDTO> reduceQuantity(String id, int quantity) {
        return null;
    }

    /*
    @Override
    @GetMapping(value = "item/category/{category}", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<ItemDTO> findByCategory(@PathVariable("category") final CategoryDTO category) {
        return this.itemService.findByCategory(category);
    }
    */

    @Override
    @GetMapping(value = "item/type/{type}", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<ItemDTO> findByItemType(@PathVariable("type") final ItemTypeDTO itemTypeDTO) {
        return this.itemService.findByItemType(itemTypeDTO);
    }



    @Override
    @GetMapping(value = "items", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<ItemDTO> listAll() {
        return this.itemService.listAll();
    }

    @Override
    @GetMapping(value = "item/types", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<ItemTypeDTO> listAllTypes() {
        return this.itemService.listAllTypes();
    }
}
